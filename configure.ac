# Configuration for mopac7.
# Copyright (c) 2003- Tommi Hassinen, Jean Brefort, Michael Banck,
# Daniel Leidert.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.

# Process this file with autoconf to produce a configure script.

AC_PREREQ([2.48])
AC_INIT([mopac7], [1.15], [ghemical-devel@bioinformatics.org])
AM_INIT_AUTOMAKE([-Wall])

##################################################################
## also see fortran/Makefile.am when you change the version number!!!
##################################################################

AC_CONFIG_SRCDIR([fortran/mopac7lib.f])

AC_CANONICAL_HOST

# Get any build/host and enable/disable flags now.
# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

# Checks for programs.
# ^^^^^^^^^^^^^^^^^^^^

AC_PROG_CC
AC_PROG_F77
AC_PROG_INSTALL
##AC_PROG_RANLIB
AC_PATH_PROG([AR], [ar], [ar])
AC_PATH_PROG([SHELL], [sh], [/bin/sh])
AM_PROG_LIBTOOL

case "$F77" in
	g77*)
		FFLAGS="$FFLAGS -fno-automatic"
	;;
	gfortran*)
		FFLAGS="$FFLAGS -std=legacy -fno-automatic"
	;;
esac

# Checks for libraries.
# ^^^^^^^^^^^^^^^^^^^^^

AC_CHECK_LIB([m], [acos])

# Here is a check whether we have libf2c or libg2c (Jean Brefort).
# If libf2c is present, it is used and both the library and the executable is built.
# If only libg2c is present, then only the library is built and the executable is not built.
##############################################################################################
#saved_flags=$CFLAGS
#CFLAGS="-Dmain=MAIN__"
#have_f2c="no"
#AC_CHECK_LIB([f2c], [main], [have_f2c="yes"; CFLAGS=$saved_flags],
#	[# If not found, try -lg2c:
#	CFLAGS=$saved_flags;
#	AC_CHECK_LIB([g2c], [main], ,
#		# Still not found, drop out:
#		AC_MSG_ERROR([Cannot find either f2c or g2c library])]
#	)
#)
#AM_CONDITIONAL(HAVE_F2C,test "$have_f2c" = "yes")

# If we only have libg2c present, we must make a local copy of f2c.h
# header file that includes g2c.h file and contains some definitions.
##############################################################################################
#if test "$have_f2c" = "no"; then
#   echo "#include <g2c.h>" > src/f2c.h
#   echo "#define VOID void" >> src/f2c.h
#   AC_MSG_WARN(mopac7 program cannot be built without f2c)
#   AC_MSG_WARN(a local copy of f2c.h header has been created in src/)
#else
#   rm -f src/f2c.h
#fi

# Checks for header files.
# ^^^^^^^^^^^^^^^^^^^^^^^^

# Checks for typedefs, structures, and compiler characteristics.
# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

AC_C_CONST
AC_C_INLINE

# Checks for library functions.
# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

AC_CONFIG_FILES([
	Makefile
	fortran/Makefile
	fortran/c_src_bak/Makefile
	fortran/c_src_f2c/Makefile
	tests/Makefile
	libmopac7.pc
])
AC_OUTPUT

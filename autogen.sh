#!/bin/sh

OSTYPE=`uname -s`

AMFLAGS="--add-missing"
if test "$OSTYPE" = "IRIX" -o "$OSTYPE" = "IRIX64"; then
   AMFLAGS=$AMFLAGS" --include-deps";
fi

echo "Running libtoolize"
libtoolize --force --copy
echo "Running aclocal"
#rm -fr autom4te.cache
aclocal --force
echo "Running automake"
automake $AMFLAGS
echo "Running autoconf"
autoconf

echo "======================================"
echo "Now you are ready to run './configure'"
echo "======================================"
